# Автоматизация Selenium на Python
**Уроки и материалы по курсу "Автоматизация Selenium на Python"**

Сам курс [по ссылке](https://www.youtube.com/playlist?list=PLbuh2pN46AEtSlQdsVn4krLki8Cte7x1S)

Так же рекомендую ["Решение задач по курсу "Автоматизация тестирования с помощью Selenium и Python"](https://github.com/psiyavush/selenium_stepik)
____
## Список кодов к урокам по разделам
### Файлы из api_test_1
* chuck - библиотека request, отправка первых запросов
* chuck_random - библиотека request, отправка запросов с принципами ООП
### Файлы из pytest_lesson_2 -> папка tests
* test_mail_1 
* test_mail_2
* test_mail_3
    - Импортирование и создание первых тестов, Знакомство с ключами для запуска тестов и анализ результата их прохождения
* conftest - фикстуры в отдельный файл, условный оператор Yield, параметр scope
* test_order - Задача очередности запуска методов с помощью PyTest
### Файлы из selenium_lesson_3
* selenium_lesson_1 - Открытие браузера с помощью Selenium
* selenium_lesson_2 - Заполнение полей с помощью метода send_keys
* selenium_lesson_3 - Поиск локаторов. Что такое XPATH?
* selenium_test_1 - Позитивное тестирование. Первые тест с проверкой входа на заголовок и url
* selenium_test_2 - Негативное тестирование. Очистка полей формы. перезагрузка страницы
* selenium_test_3 - Имитация нажатия клавиш клавиатуры
* selenium_test_4 - Создание скриншотов страницы
* selenium_test_5 - Скроллинг экрана и наводка на определенный элемент
* selenium_test_6 - Работа со скрытым меню. Перемещение по стрелкам браузера
* selenium_test_7 - Взаимодействие с Check Box
* selenium_test_8 - Взаимодействие с Radio button
* selenium_test_9 - Двойной клик и клик левой клавиши мыши
* selenium_test_10 - Smoke testing всего бизнес пути
* selenium_test_11 - Работа с календарем и датой
* selenium_test_12 - Взаимодействие с ползунком браузера
* selenium_test_13 - Отработка исключений try-except 