# Заполнение полей с помощью метода send_keys
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome() # Chrome
driver.get('https://www.saucedemo.com/')
driver.maximize_window()

user_name = driver.find_element(By.ID, "user-name") #ID
user_name.send_keys('standard_user')

time.sleep(5)
driver.close()