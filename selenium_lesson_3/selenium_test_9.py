from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://demoqa.com/buttons'
driver.get(base_url)
driver.maximize_window()

action =ActionChains(driver)

# Кликнем на кнопку Double Click Me
double_click_btn = driver.find_element(By.ID, 'doubleClickBtn')
action.double_click(double_click_btn).perform()
time.sleep(3)

# Проверка успешного нажатия
text_double_click = driver.find_element(By.ID, 'doubleClickMessage')
assert 'You have done a double click' == text_double_click.text
print('Double Click is Good')

# Кликнем на кнопку Right Click Me
right_click_btn = driver.find_element(By.ID, 'rightClickBtn')
action.context_click(right_click_btn).perform()

time.sleep(3)

# Проверка успешного нажатия
text_right_click = driver.find_element(By.ID, 'rightClickMessage')
assert 'You have done a right click' == text_right_click.text
print('Right Click is Good')


driver.quit()
