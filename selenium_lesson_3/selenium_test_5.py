import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
# driver.maximize_window()

login_standard_user = "standard_user"
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name.send_keys(login_standard_user)
print('Input Login')

password = driver.find_element(By.XPATH, "//input[@id='password']")
password.send_keys(password_all)
print('Input Password')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()
print('Click Login Button')
time.sleep(5)

# Можно использовать скроллинг по пискселям, но это костыль:
# execute_script подключает JS скрипт
# scrollTo(X, Y) X - по горизонтали, Y - по вертикали
# driver.execute_script('window.scrollTo(0, 200)')

# Можно как автор курса с подключением ActionChains:
# from selenium.webdriver import ActionChains
# action = ActionChains(driver) # управление драйвером
# red_t_short = driver.find_element(By.XPATH, "//button[@id='add-to-cart-test.allthethings()-t-shirt-(red)']")
# action.move_to_element(red_t_short).perform()

# А МОЖНО СДЕЛАТЬ СКРОЛЛИНГ ДО НУЖНОГО ЭЛЕМЕНТА ТАК:
red_t_short = driver.find_element(By.XPATH, "//button[@id='add-to-cart-test.allthethings()-t-shirt-(red)']")
driver.execute_script("return arguments[0].scrollIntoView(true);", red_t_short)

now_date = datetime.datetime.utcnow().strftime("%Y.%m.%d.%H.%M.%S")
name_screenshot = 'screenshot_' + now_date + '.png'
driver.save_screenshot('D:\\Мои документы\\IT обучение\\Auto_Test\\selenium_lesson_3\\screen\\' + name_screenshot)

time.sleep(3)
driver.quit()

