from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://demoqa.com/dynamic-properties'
# driver.implicitly_wait(5)
driver.get(base_url)
driver.maximize_window()

# Если определенный элемент не успевает загрузиться, а код уже выполняется, то
# произойдет ошибка (например элемент не найден) Эту ошибку можно записать ои обработать

try:
    visible_button = driver.find_element(By.ID, 'visibleAfter')
    visible_button.click()
except NoSuchElementException as exception:
    # Если произошла такая ошибка, выполняем этот код
    print('NoSuchElementException exception')
    time.sleep(5)
    visible_button = driver.find_element(By.ID, 'visibleAfter')
    visible_button.click()
    print('Click Visible Button')

# Отловим ошибку на assert
base_url = 'https://demoqa.com/radio-button'
driver.get(base_url)

radio_button = driver.find_element(By.XPATH, '//label[@for="yesRadio"]')
radio_button.click()
try:
    text_after_click = driver.find_element(By.CLASS_NAME, 'text-success')
    assert text_after_click.text == 'No'
except AssertionError as exception:
    print('AssertionError exception')
    driver.refresh()
    radio_button = driver.find_element(By.XPATH, '//label[@for="yesRadio"]')
    radio_button.click()
    text_after_click = driver.find_element(By.CLASS_NAME, 'text-success')
    assert text_after_click.text == 'Yes'
    print("test is good")

time.sleep(3)
driver.close()

