from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
driver.maximize_window()

login_standard_user = "standard_user"
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name.send_keys(login_standard_user)
print('Input Login')

password = driver.find_element(By.XPATH, "//input[@id='password']")
password.send_keys(password_all)
print('Input Password')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()
print('Click Login Button')

# Проверка успешного вхождения на страницу покупок, по заголовку на странице
text_products = driver.find_element(By.XPATH, "//span[@class='title']")
value_text_products = text_products.text
assert value_text_products == 'Products'
print('Тест на заголовок пройден')

# Проверка успешного вхождения на страницу покупок, по url
url = 'https://www.saucedemo.com/inventory.html'
get_url = driver.current_url
assert get_url == url
print('Тест на url пройден')

# На практике достаточно оставить один из видов проверки (лучше по url)

time.sleep(5)
driver.close()

# driver.close() - Этот метод закрывает только активное окно браузера
# driver.quit() - Этот метод полностью завершает сессию WebDriver, закрывает все открытые окна и вкладки браузера,
# а также освобождает ресурсы, используемые драйвером.
