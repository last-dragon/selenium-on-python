from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://demoqa.com/checkbox'
driver.get(base_url)
driver.maximize_window()

# Ставим галочку
# check_box = driver.find_element(By.XPATH, "//span[@class='rct-checkbox']")
# check_box.click()
# time.sleep(3)

# Раскрываем список и ставим галочку в одном из квадратиков
checkbox_arrow = driver.find_element(By.XPATH, '//button[@aria-label="Toggle"]')
checkbox_arrow.click()
print('List is opening')
time.sleep(3)
check_box = driver.find_element(By.XPATH, '//*[@id="tree-node"]/ol/li/ol/li[2]/span/label/span[1]')
check_box.click()
print('checkbox is checked')
time.sleep(3)
# Проверка успешного нажатия
text_after_click = driver.find_element(By.ID, 'result')
assert 'documents' in text_after_click.text
print('Check is Good')
driver.quit()
