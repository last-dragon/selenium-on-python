from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
driver.maximize_window()

# Вход в магазин
login_standard_user = "standard_user"
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name.send_keys(login_standard_user)
print('Input Login')

password = driver.find_element(By.XPATH, "//input[@id='password']")
password.send_keys(password_all)
print('Input Password')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()
print('Click Login Button')

# Проверка успешного вхождения на страницу покупок, по url
url = 'https://www.saucedemo.com/inventory.html'
get_url = driver.current_url
assert get_url == url
print('Тест на url Страница товаров пройден')

# Собираем данные о первом товаре и добавляем его в корзину
"""INFO Product #1"""
product_1 = driver.find_element(By.ID, "item_4_title_link").text
print(product_1)
price_1 = driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[1]/div[2]/div[2]/div').text
print(price_1)
select_product_1 = driver.find_element(By.ID, "add-to-cart-sauce-labs-backpack")
select_product_1.click()
print('Select Product 1')

cart = driver.find_element(By.ID, "shopping_cart_container")
cart.click()
print('Enter Cart')

# Собираем информацию о товаре в корзине и проверяем с информацией собранной на этапе добавления
"""INFO Cart Product #1"""
cart_product_1 = driver.find_element(By.ID, "item_4_title_link").text
print(cart_product_1)
assert product_1 == cart_product_1
print('INFO Cart Product #1 GOOD')

cart_price_1 = driver.find_element(By.XPATH,
                                   '//*[@id="cart_contents_container"]/div/div[1]/div[3]/div[2]/div[2]/div').text
print(cart_price_1)
assert price_1 == cart_price_1
print('INFO Cart Price #1 GOOD')

# Переходим к оформлению заказа
checkout = driver.find_element(By.ID, "checkout")
checkout.click()
print('Click Checkout')

"""SELECT User INFO"""
first_name = driver.find_element(By.ID, "first-name")
first_name.send_keys('Ivan')
print('Input First Name')

last_name = driver.find_element(By.ID, "last-name")
last_name.send_keys('Petrov')
print('Input Last Name')

zip_code = driver.find_element(By.ID, "postal-code")
zip_code.send_keys('198296')
print('Input ZIP Code')

button_continue = driver.find_element(By.ID, "continue")
button_continue.click()
print('Click Continue')

# Собираем информацию о товаре на финальной стадии и проверяем с ранее собранной на начальных этапах
"""INFO Finish Product #1"""
finish_product_1 = driver.find_element(By.ID, "item_4_title_link").text
print(finish_product_1)
assert product_1 == finish_product_1
print('INFO Finish Product #1 GOOD')

finish_price_1 = driver.find_element(By.XPATH,
                                     '//*[@id="checkout_summary_container"]/div/div[1]/div[3]/div[2]/div[2]/div').text
print(finish_price_1)
assert price_1 == finish_price_1
print('INFO Finish Price #1 GOOD')

summary_price = driver.find_element(By.XPATH, '//*[@id="checkout_summary_container"]/div/div[2]/div[6]').text
print(summary_price)
assert price_1 in summary_price
print('Total Summary Price Good')

# Завершаем заказ
finish_button = driver.find_element(By.ID, "finish")
finish_button.click()
# Проверка успешного вхождения на страницу окончания покупок, по url
finish_url = 'https://www.saucedemo.com/checkout-complete.html'
get_finish_url = driver.current_url
assert finish_url == get_finish_url
print('Тест на url Страница окончания покупок пройден')

# Возвращение на главную страницу товаров
back_button = driver.find_element(By.ID, "back-to-products")
back_button.click()

# Проверка успешного вхождения на страницу покупок, по url
get_url = driver.current_url
assert get_url == url
print('Тест на url Страница товаров пройден')

driver.close()
