from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.tinkoff.ru/loans/auto-loan/'
driver.implicitly_wait(5)
driver.get(base_url)
driver.maximize_window()

# Спускаемся в нужную форму
go_to_form = driver.find_element(By.XPATH, '//form[@autocomplete="off"]')
driver.execute_script("return arguments[0].scrollIntoView(true);", go_to_form)
time.sleep(5)

# Получаем текущие значение ползунка
value = driver.find_element(By.XPATH, '//input[@data-qa-type="uikit/inlineInput.input"]')
print(value.get_attribute('value'))

# Подключаем библиотеку работы с мышью
action = ActionChains(driver)
price = driver.find_element(By.XPATH, '//div[@aria-valuemin="100000"]')
# click_and_hold - функция кликнуть и держать
# move_by_offset(X, Y) - X - движение по горизонтали, Y - по вертикали
# release() - отпустить мышь
# perform() - сохранить результат
action.click_and_hold(price).move_by_offset(20, 0).release().perform()

# выводим новое значение ползунка
print(value.get_attribute('value'))

time.sleep(5)
driver.close()

