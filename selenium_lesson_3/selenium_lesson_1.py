# Открытие браузера с помощью Selenium
from selenium import webdriver
import time

driver = webdriver.Chrome() # Chrome
# driver = webdriver.Firefox() #Firefox
driver.get('https://www.saucedemo.com/')
driver.maximize_window()

time.sleep(5)
driver.close()