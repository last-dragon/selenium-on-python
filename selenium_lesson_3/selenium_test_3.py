from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
import time

base_url = "https://www.saucedemo.com/"
driver = webdriver.Chrome()
driver.get(base_url)
driver.maximize_window()

login_standard_user = 'standard_user'
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, '//input[@id="user-name"]')
user_name.send_keys(login_standard_user)
print("Input login")

# Удаляем последний символ из введенного поля login
user_name.send_keys(Keys.BACKSPACE)
time.sleep(2)
# Удаляем 2 символа из введенного поля login
user_name.send_keys(Keys.BACKSPACE*2)
time.sleep(2)
# Возвращаем стертое
user_name.send_keys('ser')

passport = driver.find_element(By.XPATH, '//input[@id="password"]')
passport.send_keys(password_all)
print("Input Passport")
# После ввода пароля нажимаем на кнопку ENTER (может быть задан как отдельный тест)
passport.send_keys(Keys.RETURN)  # RETURN === ENTER

# Работа с фильтром товаров (выпадающий список)
filter = driver.find_element(By.XPATH, '//select[@data-test="product_sort_container"]')
filter.click()
filter.send_keys(Keys.ARROW_DOWN*2)
time.sleep(2)
filter.send_keys(Keys.RETURN)
print('click Filter')

time.sleep(3)
driver.quit()

