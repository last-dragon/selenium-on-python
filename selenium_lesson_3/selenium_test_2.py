from selenium import webdriver
from selenium.webdriver.common.by import By
import time

base_url = "https://www.saucedemo.com/"
driver = webdriver.Chrome()
driver.get(base_url)
driver.maximize_window()

# заведомо вводим неправильные данные
login_standard_user = 'standard_use'
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, '//input[@id="user-name"]')
user_name.send_keys(login_standard_user)
print("Input login")
passport = driver.find_element(By.XPATH, '//input[@id="password"]')
passport.send_keys(password_all)
print("Input Passport")
button_login = driver.find_element(By.XPATH, '//input[@id="login-button"]')
button_login.click()
print("Click Login Button")
time.sleep(3)
warning_text = driver.find_element(By.XPATH, '//h3[@data-test="error"]')
value_warning_text = warning_text.text

# проверка, что выскакивает ошибка
assert value_warning_text == 'Epic sadface: Username and password do not match any user in this service'
print('Good Test')

# Очистка полей формы по отдельности
# user_name.clear()
# passport.clear()

# Очистка все поля сразу (как пример 3 типа полей ввода)
input_elements = driver.find_elements(By.CSS_SELECTOR, 'input[type=text], input[type=password], textarea')
# Очистить каждое текстовое поле
for element in input_elements:
    element.clear()

# Для перезагрузки страницы
driver.refresh()

time.sleep(5)
driver.quit()