import datetime
from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://demoqa.com/date-picker'
driver.get(base_url)
driver.maximize_window()

# Получаем поле ввода даты
new_date = driver.find_element(By.ID, 'datePickerMonthYearInput')

"""Ручной ввод даты"""
# Пройдемся по значению new_date и удалим его
# можно и так: new_date.send_keys(Keys.BACKSPACE*10), но если количество символов измениться, то код уже не отработает
for _ in range(len(new_date.get_attribute('value'))):
    new_date.send_keys(Keys.BACKSPACE)
time.sleep(3)
new_date.send_keys("08/17/2023")  # Вводим свою
time.sleep(3)
new_date.send_keys(Keys.RETURN)
time.sleep(3)

"""Ввод сегодняшней даты по частичному значению атрибута"""
new_date.click()
date_today = driver.find_element(By.XPATH, '//div[contains(@class,"react-datepicker__day--today")]')
date_today.click()
time.sleep(3)

"""Ввод завтра с использованием локатора атрибута aria-label (на данном сайте в нем дублируется дата)"""
new_date.click()
# получаем сегодняшний день
today = datetime.datetime.utcnow().strftime("%d")
# делаем из него завтра
tomorrow = int(today) + 1
locator = '//div[@aria-label="Choose Thursday, August ' + str(tomorrow) + 'th, 2023"]'
date_tomorrow = driver.find_element(By.XPATH, locator)
date_tomorrow.click()

time.sleep(3)

