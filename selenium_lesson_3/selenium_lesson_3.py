# Поиск локаторов. Что такое XPATH?
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome() # Chrome
driver.get('https://www.saucedemo.com/')
driver.maximize_window()

# user_name = driver.find_element(By.ID, "user-name") #ID
# user_name = driver.find_element(By.NAME, "user-name") #NAME
# user_name = driver.find_element(By.XPATH, "//*[@id='user-name']") # Full XPATH
user_name = driver.find_element(By.XPATH, "//input[@id='user-name']") # обращение по id с помощью XPATH
user_name.send_keys('standard_user')

# XPath — язык запросов к элементам XML-документа.

password = driver.find_element(By.CSS_SELECTOR, "#password")
password.send_keys('secret_sauce')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()

time.sleep(5)
driver.close()