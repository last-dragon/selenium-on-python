from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
driver.maximize_window()

login_standard_user = "standard_user"
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name.send_keys(login_standard_user)
print('Input Login')

password = driver.find_element(By.XPATH, "//input[@id='password']")
password.send_keys(password_all)
print('Input Password')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()
print('Click Login Button')

# Работа со скрытым меню
menu = driver.find_element(By.XPATH, "//button[@id='react-burger-menu-btn']")
menu.click()
print('Menu Click')
time.sleep(2)
link_about = driver.find_element(By.XPATH, "//a[@id='about_sidebar_link']")
link_about.click()
# Проверка правильности перехода
url = 'https://saucelabs.com/'
get_url = driver.current_url
assert get_url == url
print('Тест на url пройден')

# Перемещение по стрелкам браузера
driver.back()
time.sleep(2)
driver.forward()


time.sleep(2)
driver.quit()

