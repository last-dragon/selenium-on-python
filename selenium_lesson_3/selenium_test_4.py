import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://www.saucedemo.com/'
driver.get(base_url)
driver.maximize_window()

login_standard_user = "standard_user"
password_all = 'secret_sauce'

user_name = driver.find_element(By.XPATH, "//input[@id='user-name']")
user_name.send_keys(login_standard_user)
print('Input Login')

password = driver.find_element(By.XPATH, "//input[@id='password']")
password.send_keys(password_all)
print('Input Password')

button_login = driver.find_element(By.XPATH, "//input[@value='Login']")
button_login.click()
print('Click Login Button')

# создаем переменную текущее время, чтобы добавить ее к названию скриншота
# (так же на практике к названию можно добавлять имя теста)
now_date = datetime.datetime.utcnow().strftime("%Y.%m.%d.%H.%M.%S")
name_screenshot = 'screenshot_' + now_date + '.png'
# Делаем скриншот
# driver.save_screenshot(name_screenshot)

# Делаем скриншот в отдельную папку
driver.save_screenshot('D:\\Мои документы\\IT обучение\\Auto_Test\\selenium_lesson_3\\screen\\' + name_screenshot)

# если не получается сохранить в папку (проблема с путями, сожно подключить встроенную библиотеку os)
# import os
# screenshot_folder = 'D:\\Мои документы\\IT обучение\\Auto_Test\\selenium_lesson_3\\screen'
# screenshot_path = os.path.join(screenshot_folder, name_screenshot)

time.sleep(3)
driver.quit()

