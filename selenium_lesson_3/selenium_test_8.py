from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
base_url = 'https://demoqa.com/radio-button'
driver.get(base_url)
driver.maximize_window()

# Кликнем на кнопку Yes
radio_button = driver.find_element(By.XPATH, '//label[@for="yesRadio"]')
# Можно также найти по тексту у радио кнопки - '//label[text()="Yes"]'
radio_button.click()
time.sleep(3)

# Проверка успешного нажатия
text_after_click = driver.find_element(By.CLASS_NAME, 'text-success')
assert 'Yes' in text_after_click.text
print('Check is Good')
driver.quit()
