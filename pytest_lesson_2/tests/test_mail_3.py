import pytest


# # то, что будут выполненною до начала теста (для этого ф-ю set_up, передаем в тестовые, как аргумент)
# @pytest.fixture()
# def set_up():
#     print("Вход в систему выполнен")
#     # то что будет выполнено после теста
#     yield
#     print("Выход из системы")

def test_sending_mail_5(set_up, some):
    print('Письмо отправлено 3')


def test_sending_mail_6(set_up, some):
    print('Письмо отправлено 3')

# pytest - запускает все файлы и показывает % выполнения
# pytest -v - запускает все функции из всех файлов и показывает % выполнения
# pytest -s - выводит названия файлов и все, что должно быть напечатано с функцией print
# pytest -s -v - видим все файлы и тесты которые прошли/не прошли, видим все, что должно быть напечатано
# pytest -s -v test_mail_1.py - если хотим запустить конкретный файл
